---
title: Récolte de questions QCM
author: participants au DIU EIL Lille
date: Mercredi 26 juin 2019 - 20:32:41
---

# Thème 1 - Représentation des données : types et valeurs de base

## question 1.1 ##

Combien d'entiers positifs ou nuls peut-on représenter en machine sur 32 bits ?

* [x] $`2^{32}`$
* [ ] $`2^{32-1}`$
* [ ] $`2 \times 32`$
* [ ] $`32^2`$

_Commentaire : qcm 1.1 du sujet 0_



## question 1.2 ##

Comment est représenté la valeur 17 en binaire ?

* [x] 0001 0001
* [ ] 0001 0111
* [ ] 0010 0111
* [ ] 00010010



## question 1.3 ##

Combien vaut 11 en binaire ?

* [x] 0000 1011
* [ ] 0001 0001
* [ ] 0000 0011
* [ ] 0011 0000



## question 1.4 ##

Le nombre 20 a une représentation binaire contenant au moins

* [x] 5 bits
* [ ] 4 bits
* [ ] 6 bits
* [ ] 7 bits



## question 1.5 ##

Comment le nombre -3 est-il représenté en mémoire avec le complément à 2 et sur 4 bits ?

* [x] 1101
* [ ] 0011
* [ ] 1100
* [ ] 0100



## question 1.6 ##

On souhaite écrire le nombre flottant 0,25 sous la forme (-1)<sup>s</sup>×2<sup>e</sup>×m.
Quelles valeurs faut-il donner à s, e et m ?

* [x] s = 0 ; e = -2 ; m = 1
* [ ] s = 2 ; e = 0 ; m = 1
* [ ] s = 0 ; e = -2 ; m = 25
* [ ] s = 0 ; e = 2 ; m = 25



## question 1.7 ##

Le nombre entier positif écrit en binaire sous la forme 110 à pour écriture en base 10 :

* [x] 6
* [ ] 110
* [ ] 3
* [ ] 11



## question 1.8 ##

Le nombre 9 s'écrit en binaire sous la forme :

* [x] 1101
* [ ] 9
* [ ] 111111111
* [ ] 1001



## question 1.9 ##

Combien de bits sont nécessaires pour écrire en binaire le nombre 33 ?

* [x] 5
* [ ] 2
* [ ] 3
* [ ] 4



## question 1.10 ##

Parmi ces quatre nombres, lequel a une représentation exacte en binaire ?

* [x] 0,5
* [ ] 0,1
* [ ] 1/3
* [ ] 0,3



## question 1.11 ##

La variable x contient la valeur 3 et la variable y contient la valeur 4. Parmi les expressions suivantes, laquelle renvoie True ?

* [x] (x == 3) or (y == 5)
* [ ] (x == 3) and (y == 5)
* [ ] (x != 3) or (y == 5)
* [ ] y < 4



## question 1.12 ##

Parmi les représentations ci-dessous laquelle est une représentation hexadécimale?

* [x] 0x1111
* [ ] 0b1111
* [ ] 0o1111
* [ ] 1111



## question 1.13 ##

Quelle est l'écriture en base 10 de la somme de 0b0101 et de 0b1001

* [x] 15
* [ ] 1102
* [ ] 12
* [ ] 0xF



## question 1.14 ##

1 octet correspond à :

* [x] 8 bits
* [ ] 8 bytes
* [ ] 8 kibi
* [ ] un nombre binaire compris entre 0 et 256



## question 1.15 ##

1 octet correspond à :

* [x] 8 bits
* [ ] 8 bytes
* [ ] 8 kibi
* [ ] un nombre binaire compris entre 0 et 256



## question 1.16 ##

L'expression not ( a and (b or c) ) renvoie FAUX (False) si :

* [x] a = 1 (True)
b = 0 (False)
c = 1 (True)

* [ ] a = 0 (False)
b = 1 (True)
c = 0 (False)
* [ ] a = 1 (True)
b = 0 (False)
c = 0 (False)
* [ ] a = 0 (False)
b = 0 (False)
c = 1 (True)



## question 1.17 ##

Parmi les quatre expressions suivantes, laquelle renvoie 1?
Rappel: 1 est renvoyé lorsque l'expression booléenne vaut True. 

* [x] True or (True and False) 
* [ ] True and (True or False)
* [ ] False or (True and False)
* [ ] False and (True and False)

_Attention : la valeur de la seconde expression est également `True`._

## question 1.18 ##

Lequel de ces noms ne correspond pas à un systéme d'encodages de texte?

* [x] FAT-32
* [ ] UTF-8
* [ ] ASCII
* [ ] UTF-32



## question 1.19 ##

Cherchez l'intrus dans les représentations suivantes

* [x] AO
* [ ] A
* [ ] 10
* [ ] 1010

_Attention : distinguer O (o majuscule) et 0 (zéro)_

## question 1.20 ##

Quelle est le résultat de l'addition binaire suivante $`1011_{2}`$ + $`1000_{2}`$

* [x] $`100_{2}`$
* [ ] $`10011_{2}`$
* [ ] $`7`$
* [ ] $`11_{2}`$



## question 1.21 ##

Quelle est l'évaluation de l'expression suivante en Python :
0.1 + 0.3 == 0.4

* [x] False
* [ ] True
* [ ] 0.4
* [ ] true



## question 1.22 ##

Sur combien d'octets a été codé la chaîne de caractères suivante en utf-8 :
_égalités_

* [x] 10
* [ ] 8
* [ ] 16
* [ ] 1



## question 1.23 ##

Quelle est la table booléenne correspondant à l'expression suivante :
(a and b) or c

* [x]

    |a  |b  |c  |expression|
    |--|--|--|:--------:|
    |0  |1 | 0 | 0              |
    |0  |1 | 1 | 1              |
    |1  |0 | 0 | 0              |
    |1  |0 | 1 | 1              |
    |0  |0 | 0 | 0              |
    |0  |0 | 1 | 1              |
    |1  |1 | 0 | 1              |
    |1  |1 | 1 | 1              |

* [ ]

    |a  |b  |c  |expression|
    |--|--|--|:--------:|
    |0  |1 | 0 | 0              |
    |0  |1 | 1 | 1              |
    |1  |0 | 0 | 0              |
    |1  |0 | 1 | 1              |
    |0  |0 | 0 | 0              |
    |0  |0 | 1 | 0              |
    |1  |1 | 0 | 1              |
    |1  |1 | 1 | 0              |
    
* [ ]
    
    |a  |b  |c  |expression|
    |--|--|--|:--------:|
    |0  |1 | 0 | 1              |
    |0  |1 | 1 | 1              |
    |1  |0 | 0 | 1              |
    |1  |0 | 1 | 1              |
    |0  |0 | 0 | 0              |
    |0  |0 | 1 | 0              |
    |1  |1 | 0 | 1              |
    |1  |1 | 1 | 1              |
    
* [ ]
    
    |a  |b  |c  |expression|
    |--|--|--|:--------:|
    |0  |1 | 0 | 0              |
    |0  |1 | 1 | 0              |
    |1  |0 | 0 | 0              |
    |1  |0 | 1 | 0              |
    |0  |0 | 0 | 1              |
    |0  |0 | 1 | 1              |
    |1  |1 | 0 | 0              |
    |1  |1 | 1 | 0              |

## question 1.24 ##

Quelle valeur binaire correspond à la somme des deux entiers suivants exprimés en binaire ?
0b10000000 + 0b11101

* [x] 0b10011101
* [ ] 0b1100010
* [ ] 0b101101000
* [ ] 0b111101


## question 1.25 ##

Quelle table correspond à l'expression booléenne :
(not a) or b

* [x]

    |a  |b  |(not a) or b|
    |--|--|:--------:|
    |0  |0 | 1              |
    |0  |1 | 1              |
    |1  |0 | 0              |
    |1  |1 | 1              |
* [ ]

    |a  |b  |(not a) or b|
    |--|--|:--------:|
    |0  |0 | 0              |
    |0  |1 | 0              |
    |1  |0 | 1              |
    |1  |1 | 0              |
 
* [ ]

    |a  |b  |(not a) or b|
    |--|--|:--------:|
    |0  |0 | 0              |
    |0  |1 | 1              |
    |1  |0 | 1              |
    |1  |1 | 1              |

* [ ]

    |a  |b  |(not a) or b|
    |--|--|:--------:|
    |0  |0 | 1              |
    |0  |1 | 0              |
    |1  |0 | 1              |
    |1  |1 | 1              |

## question 1.26 ##

Quelle est l'écriture hexadécimale du nombre n dont l'écriture binaire est n=1101101 ?

* [x] 6D
* [ ] D6
* [ ] 6E
* [ ] 613

_Commentaire : la question est peut-être à reformuler en mettant une barre au-dessus de 1101101, chose que je ne sais pas faire_



## question 1.27 ##

Quel est le nombre minimal de bits nécessaires pour représenter le nombre décimal 227 ?

* [x] 8
* [ ] 11
* [ ] 7
* [ ] 6



## question 1.28 ##

La somme des entiers positifs 1001 et 101 est

* [x] 1110
* [ ] 1102
* [ ] 10010101
* [ ] 1001101


## question 1.29 ##

Le codage binaire par complément à 2 sur 4 bits de -5 est : 

* [x] 1011
* [ ] 1010
* [ ] 1101
* [ ] 0110



## question 1.30 ##

Quelle est la somme en base 10 de ces deux entiers signés codés sur 4 bits par complément à 2 ? 
1001 et 0101


* [x] -2
* [ ] 2
* [ ] 14
* [ ] -6



## question 1.31 ##

Le complément à 2 de 01010101 est:

* [x] 10101011
* [ ] 01010110
* [ ] 10101010
* [ ] 10101011
* [ ] 01010101



## question 1.32 ##

Le nombre décimal 1,75 est représenté s'écrit en binaire :

* [x] 1,110
* [ ] 1,111
* [ ] 1,010
* [ ] 1,011



## question 1.33 ##

On considère la table de vérité ci-dessous :

| A | B | A □ B |
|:-:|:-:|:-----:|
| 1 | 1 |   1   |
| 1 | 0 |   0   |
| 0 | 1 |   1   |
| 0 | 0 |   1   |


D'après cette table, à quoi correspond l'expression A □ B ?


* [x] non A ou B
* [ ] A ou B
* [ ] A ou non B
* [ ] non A ou non B



## question 1.34 ##

On considère la table de vérité ci-dessous :

| A | B | A ○ B |
|:-:|:-:|:-----:|
| 1 | 1 |   1   |
| 1 | 0 |   1   |
| 0 | 1 |   0   |
| 0 | 0 |   1   |


D'après cette table, à quoi correspond l'expression A ○ B ?


* [x] A ou non B
* [ ] A ou B
* [ ] non A ou B
* [ ] non A ou non B



## question 1.35 ##

On considère la table de vérité ci-dessous :

| X | Y | X et Y |
|:-:|:-:|:------:|
| 1 | 1 |   a    |
| 1 | 0 |   b    |
| 0 | 1 |   c    |
| 0 | 0 |   d    |

Dans la troisième colonne, quelle est la valeur égale à 1 ?


* [x] a
* [ ] b
* [ ] c
* [ ] d



## question 1.36 ##

On considère la table de vérité ci-dessous :

| X | Y | X et non Y |
|:-:|:-:|:----------:|
| 1 | 1 |     a      |
| 1 | 0 |     b      |
| 0 | 1 |     c      |
| 0 | 0 |     d      |

Dans la troisième colonne, quelle est la valeur égale à 1 ?


* [x] b
* [ ] a
* [ ] c
* [ ] d



## question 1.37 ##

On considère la suite d'instructions ci-dessous :

```python
for x in range(10):

    if x % 3 == 0 or x >= 5:

        print(x, end = ' ')
```

On rappelle que pour un entier naturel `n`, l'expression `range(n)` permet d'obtenir la liste des nombres entiers de $0$ à $n-1$.

Quelle sera le texte affiché lorsqu'on exécutera ces instructions ?


* [x] 0 3 5 6 7 8 9
* [ ] 0 3 5 7 8
* [ ] 1 2 4
* [ ] 1 2 4 6 9



## question 1.38 ##

On considère la suite d'instructions ci-dessous :

```python
texte = 'Bonjour !'
if 'z' in texte:
    print('condition 1 remplie')
if 'r' in texte:
    print('condition 2 remplie')
if 'z' in texte or 'r' in texte:
    print('condition 3 remplie')
if 'z' in texte and 'r' in texte:
    print('condition 4 remplie')
```

Quelle sera le texte affiché lorsqu'on exécutera ces instructions ?


* [x] condition 2 remplie
condition 3 remplie

* [ ] condition 2 remplie
* [ ] condition 2 remplie
condition 4 remplie
* [ ] condition 2 remplie
condition 3 remplie
condition 4 remplie



## question 1.39 ##

Déterminer la proposition _vraie_.


* [x] Tous les caractères de l'encodage ASCII sont codés de la même manière
en ISO-8859-1.
* [ ] L'encodage ASCII ne contient que des lettres majuscules.
* [ ] L'encodage ISO-8859-1 ne contient que des chiffres, des symboles d'opérations arithmétiques et des symboles de ponctuation.
* [ ] L'encodage Unicode est la réunion des encodages ASCII et ISO-8859-1.

_Commentaire : Mettre la proposition correcte en dernière position._



## question 1.40 ##

Que renvoient les lignes de commande suivantes ?
``` python
>>>l=[i%3 for i in range(6)]
>>> print(l)
```

* [x] <class 'list'>
* [ ] <class 'tuple'>
* [ ] <class 'str'>
* [ ] <class 'int'>



## question 1.41 ##

Comment s'écrit 20 (décimal) en base 2?

* [x] 10100
* [ ] 1100
* [ ] 100100
* [ ] 1010



## question 1.42 ##

Quel est le complément à 2 du nombre 6?

* [x] 01010
* [ ] 00111
* [ ] 01001
* [ ] 10101

## question 1.43 ##

(0 or 1) and 1 vaut:

* [x] 1
* [ ] 2
* [ ] -1



## question 1.44 ##

L'entier relatif -1 a pour représentation binaire par complément à deux sur un octet:

* [x] 11110100
* [ ] 00001100
* [ ] 11110011
* [ ] -00001100



## question 1.45 ##

Combien d'entiers positifs ou nuls peut-on écrire sur n bits ?

* [x] $`2^n`$
* [ ] $`2^n - 1`$
* [ ] $`2^{n-1}`$
* [ ] $`n-1`$



## question 1.46 ##

Quelle est la valeur maximale d'un entier écrit sur n bits ?

* [x] $`2^n - 1`$
* [ ] $`2^{n-1}'$
* [ ] $`2n-1`$
* [ ] $`2^n`$



## question 1.47 ##

Quelle est la valeur maximale d'un entier codé sur n bits en complément à 2 ?

* [x] $`2^{n-1}-1`$
* [ ] $`2^{n-1}`$
* [ ] $`2^{n}-1`$
* [ ] $`2^{n}`$



## question 1.48 ##

Quelle est la valeur minimale d'un entier écrit sur n bits en compléments à 2

* [x] $`-2^{n-1}`$
* [ ] $`-2^{n-1}-1`$
* [ ] $`-2^{n}-1`$



## question 1.49 ##

Soit N un nombre écrit en décimal tel que $`0\le N \le 100`$.
Quel est le nombre minimal de bits pour l'écrire en binaire ?

* [x] 7
* [ ] 6
* [ ] 3
* [ ] 8



## question 1.50 ##

Convertir en décimal la valeur hexadécimale 0x11

* [x] 17
* [ ] 272
* [ ] 11
* [ ] B



## question 1.51 ##

Convertir en binaire la valeur hexadécimale 0x11

* [x] 0001 0001
* [ ] 0000 0011
* [ ] 0011 0011
* [ ] 1000 1000



## question 1.52 ##

On travaille sur des nombres écrits sur 4 bits en compléments à 2. Quel est le résultat de l'opération (-8) + (-6) ?

* [x] 2
* [ ] -14
* [ ] Cela renvoie une exception



## question 1.53 ##

On travaille sur des nombres positifs ou nuls écrits sur 4 bits. Quel est le résultat de l'opération 12 + 8 ?

* [x] 4
* [ ] 20
* [ ] 15
* [ ] Cela renvoie une exception



## question 1.54 ##

Soient 2 nombres x et y écrits sur 4 bits en compléments à 2.
x = 0b1001 et y =b0110.
Quel est le résultat de x + y ?

* [x] -1
* [ ] 1
* [ ] Cela renvoie une exception



## question 1.55 ##

Quel est le résultat de l'instruction suivante  ?

'7' + '2'

* [x] '72'
* [ ] '9'
* [ ] 72
* [ ] 9



## question 1.56 ##

Quel est le résultat de l'instruction suivante : 

'7' + 2

* [x] Cela renvoie une exception
* [ ] '72'
* [ ] '9'
* [ ] 9
* [ ] 72



## question 1.57 ##

D'après l'extrait de de table la table ASCII ci-dessous, a quel mot correspond ce code binaire :  0100001000100000101000011

| Dec | Oct | Hex | Caractère |
|-----|-----|-----|-----------|
| 065 | 101 |  41 |     A     |
| 066 | 102 |  42 |     B     |
| 067 | 103 |  43 |     C     |
| 068 | 104 |  44 |     D     |
| 069 | 105 |  45 |     E     |
| 070 | 106 |  46 |     F     |
| 071 | 107 |  47 |     G     |
| 072 | 110 |  48 |     H     |
| 073 | 111 |  49 |     I     |


* [x] BAC
* [ ] ABC
* [ ] CAB
* [ ] ACB

## question 1.58 ##

`Tab=[[1,2,3],[4,5,6],[7,8,9]]`

Quelle est la valeur de `Tab[1][2]` ?

* [x] `7`
* [ ] `2`
* [ ] `3`
* [ ] `6`



## question 1.59 ##

```
a=1
b=3
c=1
```

L'expression suivante `not(a=b or a=c)`

* [x] correspond au booléen False
* [ ] correspond au booléen True
* [ ] n'a aucun sens
* [ ] n'est pas un booléen



## question 1.60 ##

Parmi les apellations ci-dessous, laquelle correspond à un encodage de carectères

* [x] Unicode
* [ ] HTML
* [ ] CSS
* [ ] MAC


# Thème 2 - Représentation des données : types construits

## question 2.1 ##

```python
def f(L) :
    for i in range(len(L))
        L[i-1] = L[i] - 1
    return L
```

Que renvoie `f([8,4,3])` ?

* [x] [3,6,7]
* [ ] [7,3,2]
* [ ] IndexError: list index out of range
* [ ] [3,2,7]



## question 2.2 ##

Soit a = [[lig - col for col in range(2)] for lig in range (3)].
Quelle est la valeur de a[1][2] ?

* [x] -1
* [ ] 1
* [ ] 3
* [ ] None



## question 2.3 ##

Soit d le dictionnaire suivant : d = {'a' : 2, 'b' : 1}.
Quelle instruction permet de récupérer la valeur de la clé a ?

* [x] d['a']
* [ ] d[2]
* [ ] d.a
* [ ] d[a]



## question 2.4 ##

Lequel de ces objets vérifie la syntaxe d'un tuple? 

* [x] (1,2,3)
* [ ] [1,2,3]
* [ ] {1,2,3}
* [ ] "1,2,3"



## question 2.5 ##

Soit a la liste [1,2,5,6]. Quelle valeur renvoie a[1]?

* [x] 2
* [ ] 5
* [ ] 3
* [ ] 4



## question 2.6 ##

Soit a = [[ligne-colonne for ligne in range(2)]for colonne in range(3)]
Combien d'éléments contient cette matrice? 

* [x] 6
* [ ] 2
* [ ] 3
* [ ] 5



## question 2.7 ##

On dispose du tableau (ou liste) à 2 dimensions suivant:
```
scientifiques=[['EINSTEIN', 'Albert'], ['TURING','Alan'], ['DESCARTES', 'René']]
```
Je souhaite accéder à la valeur 'TURING' du tableau "scientifiques"
Dans le shell, je tape :

* [x] `scientifiques [1][0]`
* [ ] scientifiques [0][1]
* [ ] scientifiques [1][1]
* [ ] scientifiques [2][1]



## question 2.8 ##

On dispose du tableau (ou liste) à 2 dimensions suivant:
```
scientifiques=[['EINSTEIN', 'Albert'], ['TURING','Alan'], ['POINCARE', 'Henri']]
```
Je souhaite accéder à la case (ou cellule) de coordonnées 1,1 du tableau "scientifiques"
Dans le shell, je tape :  

* [x] scientifiques [1][1]
* [ ] scientifiques [1, 1]
* [ ] scientifiques (1, 1)
* [ ] scientifiques (1)(1)



## question 2.9 ##

On désire construire le tableau suivant :
```
[0, 1, 4, 9, 16, 25]
```
Quelle instruction permet de réaliser ce tableau ?  

* [x] [i**2 for i in range (6)]
* [ ] [i**2] for i in range (6)
* [ ] [i for i**2 in range (6)]
* [ ] [i for i in range (6)]



## question 2.10 ##

On désire construire le tableau suivant :
```
[0, 1, 2, 3, 4, 5]
```
Quelle instruction permet de réaliser ce tableau ?  

* [x] [i for i in range (6)]
* [ ] [i for i in range (1,6)]
* [ ] [i for i in range (5)]
* [ ] [i for i in range (0,5)]



## question 2.11 ##

On considére la liste suivante :  
l=[9, 7, 8, 6]  
Quelle expression parmi les suivantes est vraie ?


* [x] l[2] == 8
* [ ] l[1] == 9
* [ ] l[0] == 0
* [ ] l[0] != 9



## question 2.12 ##

Quelle liste correspond à l'expression suivante :  
[i for i in range(3)]

* [x] [0, 1, 2]
* [ ] [1, 2, 3]
* [ ] [3, 3, 3]
* [ ] [3]

## question 2.13 ##

Quel code python faut-il ajouter à la fin de la fonction pour qu'elle renvoie un p-uplet de 2 valeurs a et b ?

```python
def ma_fonction(x) :
    a = x * 2
    b = x * 3
```


* [x] return (a, b)
* [ ] return [a, b]
* [ ]
    return a\
    return b
* [ ] return [a:b]

## question 2.14 ##

A partir de la variable `semaine`, quel code python permet d'associer à la variable `j` la valeur `"mardi"` ?

```python
semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
```

* [x] j = semaine[1]
* [ ] j = semaine[2]
* [ ] j = semaine["mardi"]
* [ ] j = semaine("mardi")

## question 2.15 ##

Lequel de ces objets est un dictionnaire

* [x] {'a':3 , 'b':7 , 'c':5}
* [ ] [3,7,2,1]
* [ ] (3,2,1,4)
* [ ] ([3,2],[7,2],[8,1])

## question 2.16 ##

Soit le tableau défini de la manière suivante : 
tableau = [[2,3,4],[1,7,8],[9,10,11]]

Quel est la valeur de l'élément tableau[2][0]?

* [x] 9
* [ ] 4
* [ ] 1
* [ ] 7

## question 2.17 ##

Soit le tableau défini de la manière suivante : 
tableau=[4,5,6,7]

Quel est l'indice (index) du dernier élément de ce tableau ?

* [x] 3
* [ ] 7
* [ ] 4
* [ ] 6

## question 2.18 ##

Soit le tableau défini de la manière suivante :

tableau=[4,5,3,7,8]

L'instruction tableau[3]=4 va permettre de :

* [x] remplacer le 7 par un 4 dans ce tableau
* [ ] remplacer l'élément 3 par 4
* [ ] remplacer le 3ème élément par 4
* [ ] renvoyer True (vrai) si tableau[3] contient 4



## question 2.19 ##

Soit le tuple défini de la façon suivante :

tuple = (4,5,3,7,8)

L'instruction tuple[3] = 4 :

* [x] est impossible (non autorisée)
* [ ] remplace par 4 l'élément d'indice (index) 3 
* [ ] renvoie True (vrai) si tuple[3] contient 4
* [ ] remplace l'élément 3 par 4

## question 2.20 ##

On souhaite obtenir le tableau suivant:
[[1,2,3],[1,2,3],[1,2,3],[1,2,3]]

* [x] [[i for i in range (1,4)] for i in range (4) ]
* [ ] [[i for i in range (4)] for i in range (1,4) ]
* [ ] [[i for i in range (1,3)] for i in range (3) ]
* [ ] [[i for i in range (3)] for i in range (4) ]



## question 2.21 ##

Que renvoient les instructions suivantes ?
``` python
>>>l=[i%3 for i in range(6)]
>>> print(l)
```


* [x] [0, 1, 2, 0, 1, 2]
* [ ] [0,1,2,3,4,5]
* [ ] [0,3,6,9,12,15]
* [ ] [3,4,5,6,7,8]



## question 2.22 ##

Que renvoient les lignes de commandes suivantes ?
```python
>>> l=["louis",15,(0,7)]
>>> print(type(l[2]))
```

* [x] <class 'tuple'>
* [ ] <class 'str'>
* [ ] <class 'list'>
* [ ] <class 'int'>



## question 2.23 ##

Quelle est l'instruction permettant de construire la liste suivante? 
```python 
l=[0,2,4,6]
```

* [x] l=[x fr x in range(0,8,2)] 

* [ ] l=range(6)
* [ ] l=list(range(6))
* [ ] l=[x for x in range(6)]



## question 2.24 ##

```python
fruits={'pomme':5,
        'poire':2,
        'banane':7,
        'orange':4}
```
On souhaite connaître le nombre de fruits et on donne le code suivant:
```python
def nombre_de_fruits(d):
    compteur=0
    for fruits in ...:
        compteur = compteur + d[fruits]
    return compteur
```

* [x] ...=d.keys()
* [ ] ...=d.values()
* [ ] ...=d.items()
* [ ] ...=d.index()

## question 2.25 ##

Quel résultat est affiché par les commandes en langage Python suivantes:
```
>>> liste=["Alix",18,20,13,10]
>>> liste.append(8)
>>> liste[-1]
```

* [x] 8
* [ ] 10
* [ ] "Alix"
* [ ] Un message d'erreur



## question 2.26 ##

Un élève a créé une fonction `surprise` puis a saisi les commandes suivantes:
```
>>> t=[[0,1,1],[3,0,1],[-4,5,7],[0,0,0]]
>>> surprise(t)
3
```
Quel peut-être le code de la fonction `surprise`?

* [x]
    ```
	def surprise(tableau):
        return len(tableau[0])
	```

* [ ]
	```
	def surprise(tableau):
        return len(tableau)
	```

* [ ]
    ```
	def surprise(tableau):
        return tableau[0]
	```

* [ ]
	```
	def surprise(tableau):
        return tableau[0][1]
	```



## question 2.27 ##

Voici une fonction écrite en langage Python qui prend en paramètres un dictionnaire `dico` et une chaîne de caractères `chaine`.

```
def  affiche(dico, chaine):
    i=0
    for element in dico:
        if dico[element]==chaine:
            i=i+1
    return i
```

* [x] Cette fonction compte le nombre d'apparitions de `chaine` parmi les valeurs du dictionnaire `dico`. 
* [ ] Cette fonction renvoie `True` si `chaine` fait partie des valeurs du dictionnaire `dico`. 
* [ ] Cette fonction renvoie toujours 0. 
* [ ] Cette fonction renvoie le nombre de valeurs du dictionnaire `dico`.


# Thème 3 - Traitement de données en tables

## question 3.1 ##

table=[["Amiens",3.1,6.8],["Lille",5.5,6],["Paris",-1,2.7]
Qu'affiche l'instruction print(table[1][0]) ?

* [x] "Lille"
* [ ] 3.1
* [ ] "Amiens"
* [ ] 5.5



## question 3.2 ##

Quel type de fichier est adapté au traitement des données ?

* [x] CSV
* [ ] PNG
* [ ] ODT
* [ ] PDF
* [ ] JPG

## question 3.3 ##

L'extrait de code suivant permet d'importer les données des 4 colonnes d'un fichier "chicon.csv"
```python
with open(fsrc, "r", encoding="utf-8") as fs :
    ligne = fs.readline().split(';')
    # ...
```
Parmi les contenus de ces 4 fichiers, quel est celui qui pourra être lu convenablement ?

* [x] 
    ```python
    1;1;8;55
    3;1;21;23
    ```
* [ ] 
    ```python
    1 1 8 55
    3 1 21 23
    ```
* [ ] 
    ```python
    1,1,8,55
    3,1,21,23
    ```
*  [ ] 
    ```python
    1|1|8|55
    3|1|21|23
    ```


## question 3.4 ##

Un fichier CSV contient la liste des concurrents à une course à pied. La structure de ce fichier CSV est la suivante :
```python
Prénoms;Noms;Sexes
```
Un fois importées en python, les données du fichier sont stockées dans une variable `table`.
```python
table = [(1, "gaston", "lagaffe", "M"),
         (2, "jessie ", "jane", "F")]
```
Quel est le codage python qui permet d'afficher la liste de tous les concurrents hommes ("M") ?

* [x] 
    ```python
    for c in table :
        if c[3] == "M" :
            print(c)
    ```
* [ ] 
    ```python
    for c in table :
        if c["M"] == 3 :
            print(c)
    ```
* [ ] 
    ```python
    for c in table :
        if c == "M"[4] :
            print(c)
    ```
* [ ] 
    ```python
    for c in table :
        if "M" == c[4] :
            print(c)
    ```

## question 3.5 ##

on a récupéré le contenu d'un fichier cvs contenant le nom le prénom et l'age de personnes dans une table implémentée par la liste de dictionnaires
table = [{"nom": "dupont","prenom": "jean","age": 16},
             {"nom": "durant","prenom": "pierre","age": 15},
              .........
             {"nom": "doe","prenom": "jane","age": 16}]
quelle syntaxe permet de recupérer la liste des noms des personnes de 16 ans


* [x] [personne["nom"] for personne in table if personne["age"]==16]
* [ ] [personne[nom] for personne in table if personne[age]==16]
* [ ] [personne["nom"] if personne["age"]==16 for personne in table ]
* [ ] [nom if age==16 for nom,age in table ]



## question 3.6 ##

on dispose de deux tables 
t1=[{"id":1,"nom":"lui"},{"id":2,"nom":"elle"},{"id":3,"nom":"etlui"},{"id":4,"nom":"etelle"}]
t2=[{"id":1,"age":14},{"id":4,"age":15}]
on veut ajouter les ages aux valeurs de la table t1 pour former la table t0 , quelle instruction le permettrait

* [x] t0=[]
for enregistrement1 in t1:
    for enregistrement2 in t2:
        if enregistrement1["id"] == enregistrement2["id"]:
            enregistrement1["age"]=enregistrement2["age"]
    t0.append(enregistrement1)
* [ ] t0=[]
for enregistrement in t2:
    if enregistrement[id] in t1:
        t0[enregistrement["id"]]=enregistrement["age"]
* [ ] for enregistrement1 in t2:
    for enregistrement2 in t1:
        if enregistrement1[id] == enregistrement2[id]:
            t2[enregistrement1["id"]]=enregistrement2["age"]
* [ ] t0=[]
for enregistrement1 in t1:
    for enregistrement2 in t2:
        if enregistrement1["id"] == enregistrement2["id"]:
            enregistrement1["age"]=enregistrement2["age"]
    t0+={enregistrement1}



## question 3.7 ##

A partir d'un fichier _.csv_, on obtient les informations suivantes (nom de la ville, température minimale et la température maximale):
```python
l=[['Bordeaux',14,22],['Lille',10,16]
 ['Marseille',15,25],['Paris',14,20]]
```
Quelle instruction permettrait d'obtenir la température minimale à Marseille?

* [x] l[3][2]
* [ ] l[2][3]
* [ ] l[2][1]
* [ ] l[1][2]



# Thème 4 - Interactions entre l’homme et la machine sur le Web

## question 4.1 ##

Dans une page html on a un formulaire du type
```
<form method="get" action=la_page.htm">
  <p>Nom : <input type="text" name="nom" /></p>
  <p>Prénom : <input type="text" name="prenom" /><</p>
  <p><input type="submit" name="Submit" value="Soumettre" /></p>
</form>
```
quand on appuie sur le bouton Soumettre après avoir saisi Mon_Nom et Mon_Prenom que se passe t il

* [x] le navigateur ouvre l'URL la_page?nom=Mon_Nom&prenom=Mon_Prenom
* [ ] le navigateur ouvre l'URL la_page?Mon_Nom&Mon_Prenom
* [ ] le navigateur ouvre l'URL la_page en transmettant les valeurs au serveur
* [ ] rien on n'a pas créer la fonction Soumettre



## question 4.2 ##

Dans une page html on trouve le formulaire suivant
```
<form method="post" action="la_page.php">
  <p>Nom : <input type="text" name="nom" /></p>
  <p>Prénom : <input type="text" name="prenom" /><</p>
  <p><input type="submit" name="Submit" value="Soumettre" /></p>
</form> 
```
quelle instruction faut-il utiliser en php pour récupérer le nom envoyé dans la_page.php

* [x]
    ```
	<?php
	$le_nom= $_POST['nom'];
	?>
	```
* [ ]
    ```
	<?php
	$le_nom= $POST('nom');
	?>
	```
* [ ]
    ```
	<?php
	$le_nom= $la_page.php['nom'];
	?>
	```
* [ ] ce n'est pas possible car la valeur n'est transmise que dans l'URL de la page



## question 4.3 ##

dans une page on souhaite avoir un bouton qui appelle la fonction javascript fonc()
quelle instruction le permettra.

* [x] `<button onclick="fonc()">Cliquez ici</button>`
* [ ] `<button if_clicked="fonc()">Cliquez ici</button>`
* [ ] `<button  value="Cliquez ici"><a>fonc()</a></button>`
* [ ] `<button>Cliquez ici</button go=fonc()>`



## question 4.4 ##

parmi ces affirmations sur les méthodes utilisées pour transmettre les données des formulaires laquelle est fausse

* [x] La méthode POST ne peut pas transmettre plus de 16 valeurs
* [ ] La méthode GET est la valeur de méthode par défaut
* [ ] La méthode POST n'a pas de taille limite
* [ ] La méthode GET est limitée par par la taille de L'URL



## question 4.5 ##

Quelle est la syntaxe correcte pour accéder à l'objet dont l'Id est MonId ?

* [x] document.getElementById("MonId");

* [ ] window.getElementById("MonId");

* [ ] getElementById(window.["MonId"]);

* [ ] getElementById(document.["MonId"]);



## question 4.6 ##

Parmi les acronymes suivants, lequel __n'est pas__ un protocole?

* [x] HTML
* [ ] HTTP
* [ ] TCP
* [ ] UDP


# Thème 5 - Architectures matérielles et systèmes d’exploitation

## question 5.1 ##

Quelle commande utiliser pour connaitre l'endroit où se situer dans les répertoires :

* [x] pwd
* [ ] ls -al
* [ ] ctrl c
* [ ] ls



## question 5.2 ##

Parmi les propositions suivantes, identifier celle qui correspond à un système d'exploitation libre : 

* [x] GNU/Linux
* [ ] Windows
* [ ] MacOS
* [ ] Java



## question 5.3 ##

**Pour afficher vos fichiers cachés, vous utilisez la commande:**

* [x] ls -a
* [ ] ls -d
* [ ] ls -c
* [ ] ls -m



## question 5.4 ##

**Pour renommer text1 en text1.old dans le même repertoire, vous utilisez la commande :**

* [x] mv text1 text1.old
* [ ] mv text1 repertoire/text2
* [ ] cp text1 text2
* [ ] cp text1 repertoire/text1.old



## question 5.5 ##

Parmi les propositions suivantes, identifier celle qui ne correspond pas au rôle d'un système d'exploitation :

* [x] Elimination des virus et malwares
* [ ] Gestion des ressources d'un ordinateur
* [ ] Gestion du système de fichier
* [ ] Gestion des échanges entre la mémoire RAM et le processeur



## question 5.6 ##

**Pour affichez le contenu du fichier /etc/group, vous utilisez la commande :**

* [x] cat /etc/group
* [ ] mkdir group
* [ ] ls -alR
* [ ] ls -al



## question 5.7 ##

Quelle proposition suivante ne correspond pas à une couche du modèle O.S.I. (Open Systems Interconnection) ?

* [x] Communication
* [ ] Session
* [ ] Transport
* [ ] Applications

_Commentaire : Rappel : Le modèle O.S.I. comporte 7 couches._

## question 5.8 ##

**Pour affichez tous les types de fichier du repertoire courant, vous utilisez la commande :**

* [x] type *
* [ ] mkdir *
* [ ] cat *
* [ ] cp text1 text2

## question 5.9 ##

**Affichez la liste des fichiers du répertoire /etc dont le nom contient la chaîne de caractères "conf", vous utilisez la commande :**

* [x] ls /etc/conf
* [ ] ls /etc/??????
* [ ] ls /etc/*.conf

## question 5.10 ##

**Que permet d’effectuer la `Touch`**

* [x] De créer un fichier vide.
* [ ] De viser un répertoire cible.
* [ ] De lire le code d’une touche du clavier.
* [ ] De créer un répertoire vide.

## question 5.11 ##

**Que permet d’effectuer `mkdir`**

* [x] De créer un répertoire vide.
* [ ] De créer un fichier vide.
* [ ] De viser un répertoire cible.
* [ ] De lire le code d’une touche du clavier.

## question 5.12 ##

quel type de configuration réseau n'existe pas

* [x] Réseau étagé
* [ ] Réseau en étoile
* [ ] Réseau maillé
* [ ] Réseau en anneau



## question 5.13 ##

Quel est le protocole de même niveau que TCP ?

* [x] UDP
* [ ] RIP
* [ ] ARP
* [ ] RCPT



## question 5.14 ##

Pour faire communiquer 2 PC avec les IP 192.168.0.1 et 172.10.0.23, je dois utiliser

* [x] Un routeur
* [ ] Un modem
* [ ] Un switch
* [ ] Internet



## question 5.15 ##

Qu'est-ce qu'une adresse MAC ?

* [x] Un identifiant attribué lors de la fabrication d'une carte réseau
* [ ] Un identifiant attribué lors de la connexion au réseau
* [ ] Une adresse IP compatible avec Mac OS
* [ ] Un protocole réseau



## question 5.16 ##

Quels types d'adresses sont réservées aux réseaux locaux ?

* [x] 192.168.xxx.xxx
* [ ] 88.xxx.xxx.xxx
* [ ] 127.0.0.1
* [ ] 255.255.xxx.xxx



## question 5.17 ##

Comment s'appelle le service qui permet de faire le lien entre une IP et un nom de domaine ?

* [x] Le dns
* [ ] L'arp
* [ ] Le http
* [ ] Internet



## question 5.18 ##

Redwan a un ordinateur qu'il fait fonctionner avec le système d'exploitation
Linux. Il vient d'enregistrer le fichier projet.py qu'il vient d'écrire
lui-même à l'aide d'un éditeur de textes.

Il ouvre un _terminal_ (c'est-à-dire un interpréteur de commandes) dans le
dossier contenant ce fichier afin d'exécuter celui-ci.

Voici les deux dernières lignes affichées dans son terminal, avec
la commande qu'il a écrite suivie du message d'erreur obtenu :

```
$ ./projet.py
bash: ./projet.py: Permission non accordée
$
```
Le caractère `$` en début de ligne désigne l'_invite de commande_ du terminal.

Redwan consulte un forum d'entraide sur lequel on lui propose plusieurs
solutions. Quelle est celle qui lui permettra ensuite d'exécuter
`projet.py` avec la même commande que précédemment ?


* [x] `$ chmod u+x projet.py`
* [ ] `$ ls -l projet.py`
* [ ] `$ sudo chown root projet.py`
* [ ] `$ touch projet.py`




## question 5.19 ##

Déborah a un ordinateur qu'elle fait fonctionner avec le système d'exploitation
Linux. Elle utilise un _terminal_ (c'est-à-dire un interpréteur de commandes)
et voici les dernières lignes qui y sont affichées.

```
$ cd programmes
$ pwd
/home/deborah/programmes
$ ls -l
-rwxrw-r-- 1 deborah deborah 1127 janvier 20 16:19 tri_selection.py
$ ./tri_selection.py
tri_selection.py : commande introuvable
$
```
Le caractère `$` en début de ligne désigne l'_invite de commande_ du terminal.
Les lignes qui commencent pas ce caractère contiennent les _commandes_
entrées par Déborah et les autres sont les _résultats_ de ces commandes.

En résumé, Déborah s'est placée dans le dossier `/home/deborah/programmes`,
puis a vérifié la présence du programme Python qu'elle a écrit :
`tri_selection.py`. Mais quand elle a essayé de l'exécuter, un message d'erreur
indique que la commande est _introuvable_.

Voici quatre propositions de commandes à écrire dans le terminal après la
série précédente pour résoudre ce problème. Déterminer celle qui
_ne pourra pas fonctionner_.


* [x]
    ```
    $ mv tri_selection.py tri_selection.exe
    $ tri_selection.exe
    ```
    
* [ ]
    ```
    $ python tri_selection.py
    ```
    
* [ ]
    ```
    $ ./tri_selection.py
    ```
    
* [ ]
    ```
    $ export PATH=$PATH:/home/deborah/programmes
    $ tri_selection.py
    ```




## question 5.20 ##

Susie a un ordinateur qu'elle fait fonctionner avec le système d'exploitation
Linux. Elle a créé un dossier de travail pour la spécialité NSI et y a placé
un dossier `projet` contenant un dossier `projet_simulation`, contenant
lui-même deux fichiers, comme on peut le voir sur le schéma ci-dessous :
```
.
└── projet
    └── projet_simulation
        ├── resultats.txt
        └── simulation.py
```

Elle veut réorganiser son dossier de travail de sorte à avoir un seul dossier
nommé `projet_simulation` contenant les deux fichiers précédents :
```
.
└── projet_simulation
    ├── resultats.txt
    └── simulation.py
```

Susie ouvre un _terminal_ (c'est-à-dire un interpréteur de commandes) dans son
dossier de travail.

Parmi les quatre combinaisons de commandes proposées ci-dessous, déterminer
celle qui _n'effectuera pas correctement_ cette réorganisation.

Le caractère `$` en début de ligne désigne _l'invite de commandes_ du terminal.


* [x]
    ```
    $ mv projet projet_simulation
    $ mv projet_simulation/projet_simulation ..
    ```
    
* [ ]
    ```
    $ cd projet
    $ mv /projet_simulation .
    $ cd ..
    $ mv projet projet_simulation
    ```
    
* [ ]
    ```
    $ mv projet/projet_simulation/* projet
    $ rmdir projet/projet_simulation
    $ mv projet projet_simulation
    ```
    
* [ ]
    ```
    $ mv projet/projet_simulation .
    $ rmdir projet
    ```




## question 5.21 ##

Déterminer _la proposition correcte_ parmi les quatre propositions suivantes :


* [x] Un système d'exploitation peut être conçu pour fonctionner sur
plusieurs types de processeurs.

* [ ] Grâce aux systèmes d'exploitation, les mêmes fichiers exécutables d'un
programme fonctionnent sur n'importe quel ordinateur.

* [ ] Pour que deux ordinateurs puissent communiquer par Internet, ils doivent commencer par déterminer quel système d'exploitation chacun utilise.

* [ ] Un système d'exploitation assure le démarrage de l'ordinateur et de ses périphériques. Ensuite il s'interrompt pour laisser fonctionner les programmes de l'utilisateur. Et il reprend son exécution en cas de mise en veille ou d'extinction de l'ordinateur ou de ses périphériques.




## question 5.22 ##

On considère un ordinateur équipé de 4 processeurs, sur lequel on a installé
un système d'exploitation multi-tâches.

Déterminer _la proposition incorrecte_ parmi les quatre propositions suivantes :

* [x] Chaque processeur fait fonctionner sa propre copie du système.
* [ ] Le système d'exploitation peut interrompre l'exécution d'un programme avant la fin de son exécution.
* [ ] > L'ensemble des programmes constituant un système d'exploitation sont exécutés en fonction des besoins et à tour de rôle.
* [ ] > Au cours de son exécution, un programme peut faire appel au système d'exploitation pour réaliser certaines tâches.


## question 5.23 ##

Quelle commande linux permet de modifier le groupe propriétaire du dossier `livres` ?

* [x] chown georges:laurent livres
* [ ] cat /etc/group
* [ ] chmod 070 livres
* [ ] chown georges livres



## question 5.24 ##

Un fichier `readme.md` possède les droits d'accès suivant :
```
-r--r--r-- 1 user2 NSI 354 juin 13 23:56 readme.md
```
Après l'exécution de la commande :
```
chmod 766 readme.md
```
Quelles sont les droits d'accès d'un utilisateur appartenant au groupe `NSI` sur le fichier `readme.md`  ?


* [x] Lire + Modifier
* [ ] Afficher + Exécuter + Supprimer
* [ ] Supprimer mais sans afficher le contenu
* [ ] Seulement lire  le contenu



## question 5.25 ##

Un répertoire `recettes` possède les droits d'accès suivant :
```
drwxr-xr-- 2 eleve1 NSI 4096 juin  13 23:06 recettes
```
Parmi les affirmations suivantes, laquelle est vraie ?


* [x] L'utilisateur `eleve1` peut créer un fichier dans ce répertoire
* [ ] L'utilisateur `eleve1` ne peut pas lister les fichiers du répertoire
* [ ] Un utilisateur membre du groupe `NSI` peut créer un fichier dans ce répertoire
* [ ] Un utilisateur `user2` qui n'est pas membre du groupe `NSI` ne peut pas lister les fichiers du répertoire



## question 5.26 ##

Un fichier script `auto.sh` possède les droits d'accès suivant :
```
-rwxr----- 1 eleve1 NSI 17 juin 13 23:14 auto.sh
```
Le fichier `/etc/group` contient (entre autre):
```
vboxsf:x:999:localuser,eleve1,user1,user2
NSI:x:1004:localuser,eleve1,user1
eleve1:x:1001:
SNT:x:1005:user2
```
Quelle commande linux donnera à l'utilisateur `user2` le droit d'exécuter le script `auto.sh`  ?


* [x] `chmod u+r,g+w,o+x auto.sh`
* [ ] `chmod u+rwx,g+rx-w,o+r-wx auto.sh`
* [ ] `chown :SNT auto.sh`
* [ ] `chmod 744 auto.sh`

## question 5.27 ##

Parmi les adresses ci-dessous, laquelle correspond à une adresse IP de type IPv4 ?

* [x] 172.16.0.1
* [ ] 255.18.0
* [ ] 32
* [ ] 172.256.0.1

# Thème 6 - Langages et programmation

## question 6.1 ##

Que renvoie _machin(3)_ si la fonction _machin_, écrite en langage Python est :
```python
def machin(a):
    if a < 7:
        return 2 * a
    else:
        return a - 1
```

* [x] 6
* [ ] 3
* [ ] 2
* [ ] 5



## question 6.2 ##

On considère le code Python suivant :

~~~python
T = ["a", "b", "c"]
~~~

Quelle-est la syntaxe correcte pour accéder au premier élément de `T` ?

* [x] `T[0]`
* [ ] `T["a"]`
* [ ] `T(0)`
* [ ] `T("a")`

## question 6.3 ##

Combien de fois l'instruction x=x+2 va-t-elle être exécutée dans le script suivant : 

```
x=4
while(x<10):
    x=x+2
```

* [x] 3 fois
* [ ] 4 fois
* [ ] 1 fois
* [ ] 5 fois


# Thème 7 - Algorithmique

## question 7.1 ##

Que renvoi cette fonction ?
```
i = 0
J = 2
while j > 0 :
    j = j - 1
    i = i + 1
return i
```

* [x] i = 2
* [ ] i = 1
* [ ] i  = 3
* [ ] i = 0



## question 7.2 ##

On considère la fonction suivante : 
```python
def mystere(liste) :
    s = 0
    for n in liste:
        s += n
    return s/len(liste)
```
Quelle est le rôle de cette fonction dont le paramètre d'entrée liste est une liste d'entiers non vide ?

* [x] Cette fonction retourne la moyenne des éléments de la liste
* [ ] Cette fonction retourne la somme des éléments de la liste
* [ ] Cette fonction retourne l'inverse de la somme des éléments de la liste
* [ ] Cette fonction retourne une erreur



## question 7.3 ##

On considère la fonction suivante : 
```python
def mystere(liste) :
    s = 0
    for n in liste:
        s += n
    return s/len(liste)
```
Quelle valeur cette fonctin renvoie-t-elle si on lui passe le paramètre [2, 8, 8] ?

* [x] 6
* [ ] Erreur
* [ ] 2
* [ ] 15



## question 7.4 ##

On considère la fonction suivante : 
```python
def mystere(liste) :
    s = 0
    for n in liste:
        s += n
    return s/len(liste)
```
Quelle valeur cette fonction renvoie-t-elle si on lui passe le paramètre [] ?

* [x] Erreur
* [ ] ''
* [ ] False



## question 7.5 ##

On considère la fonction suivante qui prend une liste non vide en paramètre.
```python
def min(liste) :
    #ligne manquante
    for val in liste:
        if val < ret:
            ret = val
    return ret
```
Par quelle instruction faut-il remplacer la 'ligne manquante' pour que la fonction renvoie la plus petit valeur de la liste passée en paramètre ?

* [x] ret = liste[0]
* [ ] ret = 0
* [ ] ret = 10000000000
* [ ] Il n'y a pas besoin d'ajouter de ligne, la fonction donne le résultat voulu telle quelle.

## question 7.6 ##

resultat = 0
for i in range (1, len(tableau) ):
    if tableau[i]>tableau[resultat]:
        resultat = i
return i
Ce script permet de déterminer :

* [x] l'indice (index) de l'élément le plus grand (maximum) du tableau
* [ ] l'élément le plus grand (maximum) du tableau
* [ ] la moyenne des éléments du tableau
* [ ] la somme des éléments du tableau



## question 7.7 ##

resultat = 0.0
for i in range (0, len(tableau) ):
    resultat = resultat + tableau[i]
return resultat / len(tableau)

Ce script permet de déterminer : 

* [x] la moyenne des éléments du tableau
* [ ] la somme des éléments du tableau
* [ ] le maximum du tableau
* [ ] le nombre d'occurrences de l'élément i dans le tableau



## question 7.8 ##

resultat = 0
for i in range (0, len(tableau) ):
    resultat = resultat + tableau[i]
return resultat 

Ce script permet de déterminer : 

* [x] la somme des éléments du tableau
* [ ] la moyenne des éléments du tableau
* [ ] le maximum du tableau
* [ ] le nombre d'occurrences de l'élément i dans le tableau



## question 7.9 ##

Pour parcourir entièrement le tableau suivant : `tableau = [5,7,8,9]`
quelle est la syntaxe de la boucle pour que l'on peut utiliser :

* [x] `for i in range (0, len(tableau) ):`
* [ ] `for i in range (5, 9):`
* [ ] `for i in range (0, len(tableau)-1 ):`
* [ ] `for i in range (1, len(tableau)-1 ):`
