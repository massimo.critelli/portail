## Illustration d'une interaction négative entre évaluation stricte et
## compositionnalité. Ici fact2 utilise la fonction zero qui rend stricte
## l'évaluation de la conditionnelle en conséquence la fonction boucle.

def fact(n):
    if n==0:
        return 1
    else:
        return n*fact(n-1)

fact(0)

def zero(n,p):
    if n==0:
        return 1
    else:
        return p

def fact2(n):
    return zero(n,n*fact2(n-1))

fact2(0)
