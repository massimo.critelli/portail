---
title: Bonnes pratiques en programmation
date: mai 2019
author: Équipe pédagogique DIU EIL Lille
---


## Objectifs

Un programme


1. doit être lisible et clair 
2. doit être découpé en petits composants faisant peu de choses, mais les faisant bien
2. doit séparer calculs et interface homme/machine
2. doit être documenté : spécification
2. doit être testé.
 
## Général vs Python

[PEP 8](https://www.python.org/dev/peps/pep-0008/) : (Python Enhancement Proposals) = Style Guide for Python Code

# Fil conducteur 

le *triangle de Pascal*

    1
	1 1
	1 2 1
	1 3 3 1
	1 4 6 4 1
	1 5 10 10 5 1
	1 6 15 20 15 6 1
	...
	
* différentes façons de décrire les nombres figurant dans ce triangle
* avec lignes et  colonnes numérotées à partir de 0, nombre figurant sur la ligne $n$
et la colonne $p$ =  coefficient binomial égal à

                         
    $$ \mathrm{binomial}(n, p) = \frac{n!}{p!.(n-p)!}$$


où $n!$ =  *factorielle* du nombre $n$, i.e. produit de tous les entiers de 1 à $n$, avec pour convention que $0! = 1$.

* but : écrire un programme qui imprime les lignes d'un triangle de Pascal.

**NB :** il ne s'agit pas ici d'étudier la question du meilleur algorithme, mais 
bien de cerner quelques défauts fréquents de programmation chez les programmeurs débutant. 

# Première version

[Pas beau !](tr_pa0.py) 

Tous les défauts sont concentrés dans ces quelques lignes :

* illisible
* incompréhensible
* indentation réduite à son minimum (heureusement en Python elle est obligatoire)
* mélanges calcul/impression



# Deuxième version

Choix des identificateurs


* [Plus longs les identificateurs !](tr_pa0bis.py) ;-)
* [Plus significatifs les identificateurs !](tr_pa1.py)
* Défauts corrigés :
  * identificateurs plus significatifs
  * indentation (PEP 8 : éviter tabulation et préférer espaces (par multiples de 4)


# Troisème version


* [Découper en fonctions et séparer calculs et affichage](tr_pa2.py)
* éviter les constants litérales ( => `N_MAX`)

# Quatrième version



* [Documenter/spécifier son programme](tr_pa3.py) : docstrings et commentaires

# Cinquième version

* [Tester son programme](tr_pa4.py) : doctests

# Ultime(?) version

* [Pouvoir réutiliser son programme ](triangle_pascal.py) : vers la programmation modulaire
* Utilisation des arguments sur la ligne de commande avec Thonny

# Deboguer

* Mode pas à pas de Thonny.

   * petits pas / grands pas
   * intéressant pour appels de fonctions : environnements différents, variables locales
* points d'arrêts



