Représentation des données
==========================

* support de présentation :
  [PDF](codage-slides.pdf) / [PDF 4 pages par page](codage-4ppp.pdf)

En version longue :

* supports de présentation du cours de _Codage de l'information_,
  2e année de licence informatique, Univ. Lille, année 2020-21,
  - [portail.fil.univ-lille1.fr/ls2/codage](https://portail.fil.univ-lille1.fr/ls2/codage)
  - supports de cours archivés dans [./codageL2/](./codageL2)

Travaux pratiques
-----------------

* [Bob Morane, idéogrammes](../bob-morane/Readme.md)
* [Codage, décodage base64](../base64/readme.md)
