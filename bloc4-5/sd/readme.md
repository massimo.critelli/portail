---
title: Types de données abstrait et structures de données
date: novembre 2019
author: Équipe pédagogique DIU EIL Lille
header-includes: |
   \usepackage{colortbl}
   \def\grando{\ensuremath{\mathcal{O}}}

---



# Récupérer les diapos

\begin{center}

{\Large \url{https://frama.link/diu-sd}}

\bigskip

\includegraphics[width=.3\textwidth]{fig/qr.png}
\end{center}

# Type de données abstrait et structures de données

- Type de données abstrait (TDA) : concept
\vspace*{2em}
- Structures de données : réalisation du concept

# Un concept : le transport

## Opérations

- Se déplacer

## Des réalisations du concept

- fauteuil roulant
- pieds
- vélo
- train
- voiture
- avion
- \dots

# Un TDA : les entiers
## Opérations
- Addition
- Soustraction
- Multiplication
- Division
- Égalité
- Supériorité
- \dots

\pause

## Des réalisations du concept
- allumettes
- binaire
- biquinaire

# Un TDA : liste
Une suite d'éléments :

- soit une suite vide ;
- soit un élément suivi d'une suite d'éléments.

\pause

## Opérations

- Ajout en tête
- Tête
- Reste
- Vacuité

\pause

## Réalisations

- Couple : tête, reste
- Tableau
- \dots

# Réalisation d'une liste avec un couple (tête, liste)

Liste `l = ('a',('b',('c',('d',('e',('f',('g',())))))))`


\begin{center}

\only<1>{\includegraphics[width=.8\textwidth]{fig/implem-liste-sc.pdf}}%
\only<2>{\includegraphics[width=.8\textwidth]{fig/implem-liste-sc-ajout.pdf}}

\end{center}


# Réalisation d'une liste avec un tableau

Liste `l = ('a',('b',('c',('d',('e',('f',('g',())))))))`


\begin{center}

\only<1>{\includegraphics[width=.8\textwidth]{fig/implem-tableau.pdf}}%
\only<2>{\includegraphics[width=.8\textwidth]{fig/implem-tableau-ajout.pdf}}

\end{center}

# Un concept ou plusieurs concepts ?

On parle *d'une* liste, mais le concept *liste* peut disposer de plus ou moins
d'opérations.

\pause
## Opérations supplémentaires
- longueur
- accès au $i$-ème élément
- rechercher
- renverser
- ajouter à la fin

Une liste peut, ou pas, disposer de certaines de ces opérations supplémentaires.

\pause
\bigskip

La spécification va impacter la réalisation

# Subtilités de réalisations de structures de données

- liste doublement chaînée ?
  - accès à la liste précédente
  
   \includegraphics[width=.7\textwidth]{fig/implem-liste-dc.pdf}
<!-- liste doublement chainée ? -->
    \pause
- stocker la longueur ?

# Faire une liste avec un couple ou un tableau ?

Il n'y a (généralement) pas une seule implémentation idéale d'un type de
données abstrait : **choisir selon ses besoins**

\pause

\begin{center}
        \begin{tabular}{cccc}
          & Tableau & Listes SC & Listes DC \\
          \hline
          \multicolumn{4}{c}{ opérations élémentaires} \\
          \hline
          ajout en tête & $\grando(n)$ & $\Theta(1)$ & $\Theta(1)$ \\
          accès au premier & $\Theta(1)$ & $\Theta(1)$ & $\Theta(1)$ \\
          accès au reste (ou au suivant) & $\Theta(1)$ & $\Theta(1)$ & $\Theta(1)$\\
          \hline
          \multicolumn{4}{c}{autres opérations basiques} \\
          \hline
          accès au précédent & $\Theta(1)$ & $\Theta(n)$ & $\Theta(1)$\\
          inserer après/avant$^1$ & $\grando(n)$ & $\Theta(1)$ &
            $\Theta(1)$ \\            
          accès au dernier & $\Theta(1)$ & $\Theta(n)$ & $\Theta(n)$\\
          \hline
          \multicolumn{4}{c}{ opérations avancées} \\
          \hline         
            chercher & $\grando(n)$ & $\grando(n)$ & $\grando(n)$ \\
            supprimer\footnote{une fois l'élément trouvé}&
            $\grando(n)$ & $\Theta(1)$ & $\Theta(1)$ \\
          \hline
          \multicolumn{4}{c}{ opération non élémentaire sur les listes}
          \\
          \hline
            accès au $i-$ème & $\Theta(1)$ & $\grando(n)$ & $\grando(n)$ \\          
          \hline
        \end{tabular}
\end{center}


# Ne pas accéder au i-ème élément d'une liste chainée !

\includegraphics[width=\textwidth]{fig/resultatexperience_el.png}


# D'autres éléments à prendre en compte en plus de la complexité

\includegraphics[width=\textwidth]{fig/resultatexperience.png}


# Un TDA : file
Une suite d'éléments : premier arrivé, premier sorti (**FIFO**: first in,
first out)

\pause

## Opérations
- enfiler
- défiler
- vacuité

\pause

## Réalisations

- liste ?
- tableau ?


# Un TDA : pile
Une suite d'éléments : premier arrivé, dernier sorti (**LIFO** : last in, first out)

\pause
## Opérations
- empiler
- dépiler
- vacuité

\pause
## Réalisations

Comment feriez-vous ?

# Un TDA : suite d'éléments

La liste, la file et la pile ont toutes été définies comme une suite
d'éléments.

\pause

## Opérations
- vacuité


# Un TDA : itérateur

Énumérer les éléments d'une suite d'éléments

## Opérations
- possède un suivant ?
- accède au suivant


# Choisir le bon type abstrait **et** la bonne structure


- Un même type abstrait peut être plus ou moins riche \bigskip
- L'efficacité d'un algorithme dépendra du bon choix du type abstrait **et**
  de la structure de données qui le met en œuvre \bigskip
- Bien lire (et bien écrire !) la documentation pour connaître le coût des
  différentes opérations

# Bibliographie

- [Problem Solving with Algorithms and Data Structures using Python](https://runestone.academy/runestone/books/published/pythonds/index.html)
